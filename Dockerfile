FROM node:16 AS base
WORKDIR /src
COPY package*.json /src/

FROM base AS prod
RUN npm i --production
COPY server.js routes.js index.html /src/
EXPOSE 3001
CMD ["npm", "start"]

FROM prod AS dev
RUN npm i
CMD ["npm", "run", "devStart"]

FROM dev AS test
COPY test/ /src/test/
CMD ["npm", "test"]
